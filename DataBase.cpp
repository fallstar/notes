#include <iostream>
#include <pqxx/pqxx>
#include DataBase.h

using namespace pqxx;
using namespace std;

DataBase::DataBase() :
{

}

int DataBase::dbconnect()
{
      try{
      connection C("dbname=test user=postgres password=miaou hostaddr=127.0.0.1 port=5432");

      if (C.is_open()) {
         cout << "Opened database successfully: " << C.dbname() << endl;
      } else {
         cout << "Can't open database" << endl;
         return 1;
      }
      C.disconnect ();
   }catch (const std::exception &e){
      cerr << e.what() << std::endl;
      return 1;
   }
   return 0;
}
