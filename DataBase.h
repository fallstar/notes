#ifndef DATABASE_H
#define DATABASE_H

class DataBase {
	public:
		DataBase();
		int dbconnect();
	private:
        std::string hostname;
        std::string username;
        std::string password;
        std::string dataBase;
        int port;
};

#endif // DATABASE_H
